from math import sqrt, pi, sin, cos
import os
import sys, slepc4py
import numpy as np

slepc4py.init(sys.argv)
from slepc4py import SLEPc
import argparse
from matplotlib import pyplot as plt
from core import (create_mesh, assemble, plot, add_dirichlet_matrix, init_matrix, assemble_mass, Jacobi, BlockJacobi, Richardson, interpolate)
from utils import (set_width_in_percent)

parser = argparse.ArgumentParser(description='Run the hdG test problem in 1D.')
parser.add_argument('--macroboundaries', help='position of the boundaries of the macrointervals', nargs='+', type=float,
                    default=[0, 0.5, 1])
parser.add_argument('--level', help='how many microintervals should we have?', type=int, default=1)
parser.add_argument('-dbv', '--dirichlet-boundary-values', help='apply dirichlet boundary values?', action='store_true')
parser.add_argument('--sigma', help='which value for sigma', type=float, default=6)
parser.add_argument('--num-smoothing-steps', help='how many jacobi iterations should we apply', nargs='+', type=int, 
                    default=[0, 1])
parser.add_argument('--smoother-type', help='which smoother should be used?', choices=['block-jacobi', 'jacobi', 'richardson'], default='jacobi')
parser.add_argument('--plot-type', help='which type of plot do we want?', choices=['evolution', 'rates'], default='evolution')
args = parser.parse_args()

macro_boundaries = args.macroboundaries

N = 2**args.level + 1
M = len(macro_boundaries) - 1

coords = create_mesh(macro_boundaries, N, M)

sigma = args.sigma

matrix = assemble(sigma, coords, N, M)

if args.dirichlet_boundary_values:
    add_dirichlet_matrix(matrix, N, M)


def apply_jacobi(smoother_type, num_smoothing_steps, u, f):
    u = u.copy()
    if num_smoothing_steps <= 0:
        return u, []
    if smoother_type == 'block-jacobi':
        solver = BlockJacobi(M-1, N, weight=2./3., atol=1e-15, num_smoothing_steps=num_smoothing_steps, verbose=False)
    elif smoother_type == 'jacobi':
        solver = Jacobi(weight=2./3., atol=1e-15, num_smoothing_steps=num_smoothing_steps, verbose=False)
    elif smoother_type == 'richardson':
        solver = Richardson(not args.dirichlet_boundary_values, atol=1e-15, num_smoothing_steps=num_smoothing_steps, verbose=False)
    solver.setup(matrix)
    conv_rate, res_norm, all_rates = solver.solve(u, f)
    print('a', solver.solve(u, f))
    return u, all_rates


colors = ['red', 'green', 'blue', 'orange']
linewidths = [4., 3., 2., 1.]

u, f = matrix.getVecs()

set_width_in_percent(0.75, pgf=False)


def initial_value(x, _):
    acc = 0 
    frequencies = [1, 2, N/4, N/2] 
    amplitudes = [1, 1, 1, 1] 
    for f, a in zip(frequencies, amplitudes):
        acc += a*sin(2*pi*f*x)
    return acc

def initial_value_all_frequencies(x, _):
    acc = 0 
    frequencies = range(1, int(N/2))
    amplitudes = [1 for f in frequencies] 
    for f, a in zip(frequencies, amplitudes):
        acc += a*sin(2*pi*f*x)
    return acc



if args.plot_type == 'evolution':
    interpolate(initial_value, coords, u, args.level, M)
    for sidx, smoothing_steps in enumerate(args.num_smoothing_steps):
        u_smoothed, rates = apply_jacobi(args.smoother_type, smoothing_steps, u, f)
        color = colors[sidx % len(colors)]
        if smoothing_steps == 0:
            label = 'initial error'
        elif smoothing_steps == 1:
            label = 'error after 1 smoothing step'
        else:
            label = 'error after {} smoothing steps'.format(smoothing_steps)
        plot(u_smoothed, coords, N, M, color=color, label=label, linewidth=1)

    for pt in args.macroboundaries[1:-1]:
        plt.axvline(pt, color='gray', linestyle=':')

    title = ''
    name = 'smooth-error'
    if args.smoother_type == 'jacobi':
        title = 'Jacobi-Smoother'
        name += '-jacobi'
    elif args.smoother_type == 'richardson':
        title = 'Richardson-Smoother'
        name += '-richardson'

    plt.legend()
    plt.title(title)

    plt.tight_layout()
    plt.savefig('{}.pgf'.format(name))

    plt.show()
else:
    interpolate(initial_value_all_frequencies, coords, u, args.level, M)
    steps = 100000
    _, rates = apply_jacobi('jacobi', steps, u, f)
    plt.semilogx(range(1, 1+len(rates)), rates, '-', linewidth=2, label='Jacobi')

    _, rates = apply_jacobi('richardson', steps, u, f)
    plt.semilogx(range(1, 1+len(rates)), rates, '-', label='Richardson')

    title = ''

    plt.legend()
    plt.grid(True)
    plt.xlabel('iterations')
    plt.ylabel('residual-rate')
    plt.title(title)

    plt.tight_layout()
    plt.savefig('{}.pgf'.format('convergence-rate'))

    plt.show()