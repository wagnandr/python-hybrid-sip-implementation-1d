import numpy as np
import argparse
from matplotlib import pyplot as plt
from math import (sin, pi)
from core import (create_mesh, add_dirichlet_matrix, assemble, Jacobi)


parser = argparse.ArgumentParser(description='Inspect condition number and eigenvalues of the jacobi iteration matrix.')
parser.add_argument('--macroboundaries', help='position of the boundaries of the macrointervals', nargs='+', type=float,
                    default=[0, 0.5, 1])
parser.add_argument('--level', help='which level should we use?', type=int, default=3)
args = parser.parse_args()

macro_boundaries = args.macroboundaries

N = 2 ** args.level + 1
M = len(macro_boundaries) - 1

coords = create_mesh(macro_boundaries, N, M)

list_sigmas = [2**idx for idx in range(10)]
list_cond = []
list_lambda_min = []
list_lambda_max = []
for sigma in list_sigmas:
    K = assemble(sigma, coords, N, M)
    add_dirichlet_matrix(K, N, M)
    j = Jacobi(update_only_inner=False)
    j.setup(K)
    val, vec = np.linalg.eig(j._iter_matrix[:,:])
    list_cond.append(np.linalg.cond(j._iter_matrix[:,:]))
    list_lambda_min.append(np.min(val))
    list_lambda_max.append(np.max(val))

fig, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True)

ax1.loglog(list_sigmas, list_cond, '.-', label='cond')
ax2.loglog(list_sigmas, list_lambda_min, '.-', label='$\lambda_{min}$')
ax3.plot(list_sigmas, list_lambda_max, '.-', label='$\lambda_{max}$')

ax1.legend()
ax1.grid(True)
ax2.legend()
ax2.grid(True)
ax3.legend()
ax3.grid(True)
ax3.set_xlabel('penalty $\sigma$')

plt.tight_layout()
plt.show()
