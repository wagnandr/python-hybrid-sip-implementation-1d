from matplotlib import pyplot as plt
from petsc4py import PETSc
import numpy as np
from math import (sqrt)


def create_mesh(macro_boundaries, num_microintervals, num_macrointervals):
    return [list(np.linspace(macro_boundaries[idx], macro_boundaries[idx + 1], num_microintervals))
            for idx in range(num_macrointervals)]


def init_matrix(N, M):
    A = PETSc.Mat().create()
    A.setSizes([N * M, N * M])
    A.setFromOptions()
    A.setUp()
    return A


def index(n, m, num_microindices):
    return m * num_microindices + n


def assemble_laplace(matrix, coords, N, M):
    # assemble laplacian 
    # m = macro interval index
    for m in range(0, M):
        # n = micro interval index
        for n in range(0, N - 1):
            length = coords[m][n + 1] - coords[m][n]
            idx_1 = m * N + n
            idx_2 = m * N + (n + 1)
            matrix.setValue(idx_1, idx_1, length * (-1) / length * (-1) / length, addv=PETSc.InsertMode.ADD_VALUES)
            matrix.setValue(idx_2, idx_2, length * (+1) / length * (+1) / length, addv=PETSc.InsertMode.ADD_VALUES)
            matrix.setValue(idx_1, idx_2, length * (-1) / length * (+1) / length, addv=PETSc.InsertMode.ADD_VALUES)
            matrix.setValue(idx_2, idx_1, length * (-1) / length * (+1) / length, addv=PETSc.InsertMode.ADD_VALUES)


def assemble_jump(matrix, coords, N, M, sigma):
    for b in range(0, M - 1):
        idx_1 = b * N + N - 1
        idx_2 = (b + 1) * N + 0
        length = min(coords[b][N - 1] - coords[b][N - 2], coords[b + 1][1] - coords[b + 1][0])
        matrix.setValue(idx_1, idx_1, sigma * (+1) / length, addv=PETSc.InsertMode.ADD_VALUES)
        matrix.setValue(idx_2, idx_2, sigma * (+1) / length, addv=PETSc.InsertMode.ADD_VALUES)
        matrix.setValue(idx_1, idx_2, sigma * (-1) / length, addv=PETSc.InsertMode.ADD_VALUES)
        matrix.setValue(idx_2, idx_1, sigma * (-1) / length, addv=PETSc.InsertMode.ADD_VALUES)


def assemble_avg_jump(matrix, coords, N, M):
    for b in range(0, M - 1):
        idx_0 = b * N + N - 2
        idx_1 = b * N + N - 1
        idx_2 = (b + 1) * N + 0
        idx_3 = (b + 1) * N + 1
        length_left = coords[b][N - 1] - coords[b][N - 2]
        length_right = coords[b + 1][1] - coords[b + 1][0]
        # avg jump
        matrix.setValue(idx_0, idx_1, 0.5 / length_left, addv=PETSc.InsertMode.ADD_VALUES)
        matrix.setValue(idx_0, idx_2, -0.5 / length_left, addv=PETSc.InsertMode.ADD_VALUES)
        matrix.setValue(idx_1, idx_1, -0.5 / length_left, addv=PETSc.InsertMode.ADD_VALUES)
        matrix.setValue(idx_1, idx_2, 0.5 / length_left, addv=PETSc.InsertMode.ADD_VALUES)
        matrix.setValue(idx_2, idx_1, 0.5 / length_right, addv=PETSc.InsertMode.ADD_VALUES)
        matrix.setValue(idx_2, idx_2, -0.5 / length_right, addv=PETSc.InsertMode.ADD_VALUES)
        matrix.setValue(idx_3, idx_1, -0.5 / length_right, addv=PETSc.InsertMode.ADD_VALUES)
        matrix.setValue(idx_3, idx_2, +0.5 / length_right, addv=PETSc.InsertMode.ADD_VALUES)


def assemble_jump_avg(matrix, coords, N, M):
    # assemble jumps 
    # m = macro boundary 
    for b in range(0, M - 1):
        idx_0 = b * N + N - 2
        idx_1 = b * N + N - 1
        idx_2 = (b + 1) * N + 0
        idx_3 = (b + 1) * N + 1
        length_left = coords[b][N - 1] - coords[b][N - 2]
        length_right = coords[b + 1][1] - coords[b + 1][0]
        # jump avg
        matrix.setValue(idx_1, idx_0, 0.5 / length_left, addv=PETSc.InsertMode.ADD_VALUES)
        matrix.setValue(idx_2, idx_0, -0.5 / length_left, addv=PETSc.InsertMode.ADD_VALUES)
        matrix.setValue(idx_1, idx_1, -0.5 / length_left, addv=PETSc.InsertMode.ADD_VALUES)
        matrix.setValue(idx_2, idx_1, 0.5 / length_left, addv=PETSc.InsertMode.ADD_VALUES)
        matrix.setValue(idx_1, idx_2, 0.5 / length_right, addv=PETSc.InsertMode.ADD_VALUES)
        matrix.setValue(idx_2, idx_2, -0.5 / length_right, addv=PETSc.InsertMode.ADD_VALUES)
        matrix.setValue(idx_1, idx_3, -0.5 / length_right, addv=PETSc.InsertMode.ADD_VALUES)
        matrix.setValue(idx_2, idx_3, +0.5 / length_right, addv=PETSc.InsertMode.ADD_VALUES)


def assemble(sigma, coords, N, M):
    matrix = init_matrix(N, M)
    assemble_laplace(matrix, coords, N, M)
    assemble_jump(matrix, coords, N, M, sigma)
    assemble_avg_jump(matrix, coords, N, M)
    assemble_jump_avg(matrix, coords, N, M)
    matrix.assemble()
    return matrix


def assemble_mass(matrix, coords, N, M):
    # assemble laplacian 
    # m = macro interval index
    for m in range(0, M):
        # n = micro interval index
        for n in range(0, N - 1):
            length = coords[m][n + 1] - coords[m][n]
            idx_1 = m * N + n
            idx_2 = m * N + (n + 1)
            matrix.setValue(idx_1, idx_1, length / 3, addv=PETSc.InsertMode.ADD_VALUES)
            matrix.setValue(idx_2, idx_2, length / 3, addv=PETSc.InsertMode.ADD_VALUES)
            matrix.setValue(idx_1, idx_2, length / 6, addv=PETSc.InsertMode.ADD_VALUES)
            matrix.setValue(idx_2, idx_1, length / 6, addv=PETSc.InsertMode.ADD_VALUES)


def plot(vector, coords, N, M, label='', color=None, linewidth=1):
    from matplotlib import pyplot as plt
    for m in range(M):
        local_label = label if color is None or m == 0 else ''
        plt.plot(coords[m], vector.array[m * N:(m + 1) * N], '.-', color=color, linewidth=linewidth, label=local_label)


def add_dirichlet_matrix(matrix, N, M):
    matrix.zeroRows([0, N * M - 1], diag=1)


def add_constant_dirichlet_vector(vector, scalar, N, M):
    vector.setValue(0, scalar)
    vector.setValue(N * M - 1, scalar)


def add_dirichlet(matrix, rhs, bvs, coords, N, M):
    add_dirichlet_matrix(matrix, N, M)
    rhs.setValue(0, bvs(0, 0))
    rhs.setValue(N * M - 1, bvs(coords[M - 1][N - 1], M - 1))


def interpolate(f, coords, src, srcLevel, num_macroindices):
    src_num_microdofs = 2 ** srcLevel + 1
    # m = macro interval index
    for m in range(0, num_macroindices):
        # n = micro interval index
        for n in range(0, src_num_microdofs):
            src[index(n, m, src_num_microdofs)] = f(coords[m][n], m)


def inject(src, dst, srcLevel, num_macroindices, fix_dirichlet=True):
    src_num_microdofs = 2 ** srcLevel + 1
    dst_num_microdofs = 2 ** (srcLevel + 1) + 1
    for m in range(num_macroindices):
        for srcIdx in range(0, 2 ** srcLevel + 1):
            dst[index(2 * srcIdx, m, dst_num_microdofs)] = src[index(srcIdx, m, src_num_microdofs)]
        for srcIdx in range(0, 2 ** srcLevel):
            dst[index(2 * srcIdx + 1, m, dst_num_microdofs)] = 0.5 * (
                        src[index(srcIdx, m, src_num_microdofs)] + src[index(srcIdx + 1, m, src_num_microdofs)])
    if fix_dirichlet:
        dst[0] = 0
        dst[dst.getSize() - 1] = 0


def prolongate(src, dst, srcLevel, num_macroindices, fix_dirichlet=True):
    src_num_microdofs = 2 ** srcLevel + 1
    dst_num_microdofs = 2 ** (srcLevel - 1) + 1
    for m in range(num_macroindices):
        dst[index(0, m, dst_num_microdofs)] = src[index(0, m, src_num_microdofs)] + 0.5 * src[
            index(1, m, src_num_microdofs)]
        dst[index(dst_num_microdofs - 1, m, dst_num_microdofs)] = src[index(src_num_microdofs - 1, m,
                                                                            src_num_microdofs)] + 0.5 * src[
                                                                      index(src_num_microdofs - 2, m,
                                                                            src_num_microdofs)]
        for dstIdx in range(1, dst_num_microdofs - 1):
            dst[index(dstIdx, m, dst_num_microdofs)] = src[index(2 * dstIdx, m, src_num_microdofs)] + 0.5 * (
                        src[index(2 * dstIdx - 1, m, src_num_microdofs)] + src[
                    index(2 * dstIdx + 1, m, src_num_microdofs)])
    if fix_dirichlet:
        dst[0] = 0
        dst[dst.getSize() - 1] = 0


class Richardson:
    def __init__(self, symmetric, num_smoothing_steps, atol, verbose):
        self.symmetric = symmetric
        self.num_smoothing_steps = num_smoothing_steps
        self.atol = atol 
        self.verbose = verbose 

    def solve(self, x_now, rhs):
        assert self.is_setup
        return apply_jacobi_iteration(self._matrix, self._diag, self.weight, self.num_smoothing_steps, x_now, rhs, self.atol, self.verbose)

    @property
    def is_setup(self):
        return self._diag is not None

    def setup(self, matrix):
        self._matrix = matrix
        self.weight = 1./self._spectral_radius()
        diag_vec = self._matrix.getVecLeft()
        diag_vec.set(1)
        self._diag = matrix.copy()
        self._diag.zeroEntries()
        self._diag.setDiagonal(diag_vec)
    
    def _spectral_radius(self):
        import sys, slepc4py
        slepc4py.init(sys.argv)
        from slepc4py import SLEPc
        E = SLEPc.EPS()
        E.create()

        problemType = SLEPc.EPS.ProblemType.HEP if self.symmetric else SLEPc.EPS.ProblemType.NHEP

        E.setOperators(self._matrix)

        E.setProblemType(problemType)
        E.setDimensions(nev=1)
        E.setFromOptions()

        E.solve()

        k = E.getEigenpair(0)

        return k.real


class Jacobi:
    def __init__(self, weight=2. / 3., atol=1e-12, num_smoothing_steps=1, update_only_inner=False, verbose=False):
        self.weight = weight
        self.num_smoothing_steps = num_smoothing_steps
        self.atol = atol
        self.verbose = verbose
        self.update_only_inner = update_only_inner

    def setup(self, matrix):
        diag_vector = matrix.getDiagonal()
        diag_vector[:] = 1. / diag_vector[:]
        self._iter_matrix = iter_matrix = matrix.duplicate(copy=True)
        iter_matrix.diagonalScale(diag_vector)
        # get diagonal matrix
        n, _ = matrix.getSize()
        self._diag = diag = PETSc.Mat().create()
        diag.setSizes(n)
        diag.setFromOptions()
        diag.setUp()
        if self.update_only_inner:
            diag_vector.setValue(0, 0)
            diag_vector.setValue(n - 1, 0)
        diag.setDiagonal(diag_vector)
        self._matrix = matrix

    @property
    def is_setup(self):
        return self._diag is not None

    def solve(self, x_now, rhs):
        assert self.is_setup
        return apply_jacobi_iteration(self._matrix, self._diag, self.weight, self.num_smoothing_steps, x_now, rhs, self.atol, self.verbose)

class BlockJacobi:
    def __init__(self, num_microintervals, num_microdofs, weight=2./3., atol=1e-12, num_smoothing_steps=1, update_only_inner=False, verbose=False):
        self._num_macrointervals = num_microintervals
        self._num_microdofs = num_microdofs
        self.weight = weight
        self.num_smoothing_steps = num_smoothing_steps
        self.atol = atol
        self.verbose = verbose
        self.update_only_inner = update_only_inner

    def setup(self, matrix):
        self._matrix = matrix
        # setup block diagonal matrix
        self._diag = diag_block = matrix.copy()
        diag_block.zeroEntries()
        # setup inverse diagonal
        diag_vector = matrix.getDiagonal()
        diag_vector[:] = 1. / diag_vector[:]
        diag_block.setDiagonal(diag_vector)
        # setup inverse block matrices
        for b in range(0, self._num_macrointervals):
            idx_1 = b * self._num_microdofs + self._num_microdofs - 1
            idx_2 = (b + 1) * self._num_microdofs + 0
            loc_diag = matrix.getValues([idx_1, idx_2], [idx_1, idx_2])
            loc_diag_inv = np.linalg.inv(loc_diag)
            diag_block.setValues([idx_1, idx_2], [idx_1, idx_2], loc_diag_inv)
        diag_block.assemble()

    @property
    def is_setup(self):
        return self._diag is not None

    def solve(self, x_now, rhs):
        assert self.is_setup
        return apply_jacobi_iteration(self._matrix, self._diag, self.weight, self.num_smoothing_steps, x_now, rhs, self.atol, self.verbose)


def apply_jacobi_iteration(matrix, diag, weight, num_smoothing_steps, x_now, rhs, atol, verbose):
    init_res = -1
    res_norm = 0

    tmp1 = matrix.createVecLeft()
    tmp2 = matrix.createVecLeft()

    rates = []

    for idx in range(num_smoothing_steps):
        # tmp1 = Ax
        matrix.mult(x_now, tmp1)
        # tmp2 = b
        rhs.copy(tmp2)
        # tmp2 = b - Ax
        tmp2.axpy(-1, tmp1)
        prev_res_norm = res_norm
        res_norm = tmp2.norm()

        if init_res == -1:
            init_res = res_norm
        else:
            rates.append(res_norm/prev_res_norm)

        # tmp1 = D^{-1} ( b- Ax )
        diag.mult(tmp2, tmp1)
        # x_now = x + omega D^{-1} (b-Ax)
        x_now.axpy(weight, tmp1)
        # attention: this is the residuum of the last iteration!
        if verbose:
            print('iter {} with {}'.format(idx, res_norm))
        # stop if we are smaller than the given tolerance
        if res_norm <= atol:
            res_rate = (res_norm / init_res) ** (1/(idx+1)) 
            return res_rate, res_norm, rates

    # tmp1 = Ax
    matrix.mult(x_now, tmp1)
    # tmp2 = b
    rhs.copy(tmp2)
    # tmp2 = b - Ax
    tmp2.axpy(-1, tmp1)
    prev_res_norm = res_norm
    res_norm = tmp2.norm()

    rates.append(res_norm/prev_res_norm)

    res_rate = (res_norm / init_res) ** (1/(num_smoothing_steps))

    return res_rate, res_norm, rates


class ErrorCalculator:
    def __init__(self, mass_matrix, coords, level, num_macrointervals):
        self._u_exact = mass_matrix.getVecRight()
        self._error = mass_matrix.getVecRight()
        self._error_prime = mass_matrix.getVecRight()
        self._mass_matrix = mass_matrix
        self._coords = coords
        self._level = level
        self._num_macrointervals = num_macrointervals

    def calculate(self, vector, function):
        interpolate(function, self._coords, self._u_exact, self._level, self._num_macrointervals)
        self._error.setValues(range(self._mass_matrix.getSize()[0]), (self._u_exact[:] - vector[:]) ** 2)
        self._mass_matrix.mult(self._error, self._error_prime)
        return sqrt(self._error_prime.sum())


class LUSolver:
    def setup(self, matrix):
        self._matrix = matrix

    @property
    def is_setup(self):
        return self._matrix is not None

    def solve(self, u, f):
        assert self.is_setup
        ksp = PETSc.KSP()
        ksp.create(PETSc.COMM_WORLD)
        ksp.setType('preonly')
        ksp.getPC().setType('lu')
        ksp.setOperators(self._matrix)
        ksp.setFromOptions()
        ksp.solve(f, u)


class MultigridSolver:
    def __init__(self, max_level, macro_boundaries, sigma, fct_rhs, fct_bvs):
        self.num_macrointervals = len(macro_boundaries) - 1
        self.num_microdofs = [2 ** level + 1 for level in range(max_level + 1)]
        self.list_meshes = [create_mesh(macro_boundaries, num_dofs, self.num_macrointervals) for num_dofs in
                            self.num_microdofs]
        self.list_K = [assemble(sigma, mesh, num_dofs, self.num_macrointervals) for num_dofs, mesh in
                       zip(self.num_microdofs, self.list_meshes)]
        self.list_u = [K.getVecLeft() for K in self.list_K]
        self.list_f = [K.getVecRight() for K in self.list_K]
        self.list_residual = [K.getVecRight() for K in self.list_K]
        self.list_tmp = [K.getVecRight() for K in self.list_K]
        # create initial rhs
        f_prime = self.list_K[-1].getVecLeft()
        interpolate(fct_rhs, self.list_meshes[-1], f_prime, max_level, self.num_macrointervals)
        mass_matrix = init_matrix(self.num_microdofs[-1], self.num_macrointervals)
        assemble_mass(mass_matrix, self.list_meshes[-1], self.num_microdofs[-1], self.num_macrointervals)
        mass_matrix.assemble()
        mass_matrix.mult(f_prime, self.list_f[-1])
        add_dirichlet(self.list_K[-1], self.list_f[-1], fct_bvs, self.list_meshes[-1], self.num_microdofs[-1],
                      self.num_macrointervals)
        # add bcs for u 
        self.list_u[-1].setValue(0, fct_bvs(macro_boundaries[0], 0))
        self.list_u[-1].setValue(self.num_macrointervals*self.num_microdofs[-1]-1, fct_bvs(macro_boundaries[-1], len(macro_boundaries)-1))
        # add zero boundary conditions for the rest
        for K, f, mesh, num_microdofs in zip(self.list_K[:-1], self.list_f[:-1], self.list_meshes[:-1],
                                             self.num_microdofs[:-1]):
            add_dirichlet(K, f, lambda x, _: 0, mesh, num_microdofs, self.num_macrointervals)
        # init smoother
        self.list_smoother = [Jacobi(update_only_inner=True) for _ in self.list_K]
        [smoother.setup(K) for K, smoother in zip(self.list_K, self.list_smoother)]
        # coarse grid solver
        self.coarse_level = 1
        self.coarse_grid_solver = LUSolver()
        self.coarse_grid_solver.setup(self.list_K[self.coarse_level])
        # properties
        self.two_grid = False

    def solve_recursively(self, level):
        # solve on coarse grid
        if level == self.coarse_level:
            self.coarse_grid_solver.solve(self.list_u[level], self.list_f[level])
            return

        # pre-smoothing:
        self.list_smoother[level].solve(self.list_u[level], self.list_f[level])

        # restriction:
        # tmp = A u
        self.list_K[level].mult(self.list_u[level], self.list_tmp[level])
        # residual = f
        self.list_f[level].copy(self.list_residual[level])
        # residual = f - Ax
        self.list_residual[level].axpy(-1, self.list_tmp[level])
        prolongate(self.list_residual[level], self.list_f[level - 1], level, self.num_macrointervals)

        # reset previous solution to zero
        self.list_u[level-1].zeroEntries()
        # coarse grid solver:
        if self.two_grid:
            solver = LUSolver()
            solver.setup(self.list_K[level - 1])
            solver.solve(self.list_u[level - 1], self.list_f[level - 1])
        else:
            self.solve_recursively(level - 1)

        # prolongation:
        inject(self.list_u[level - 1], self.list_tmp[level], level - 1, self.num_macrointervals)

        # coarse grid correction
        self.list_u[level].axpy(1, self.list_tmp[level])

        # post-smoothing:
        self.list_smoother[level].solve(self.list_u[level], self.list_f[level])

    def solve(self):
        self.solve_recursively(len(self.list_f) - 1)
        return self.list_u[-1]


class ResiduumCalculator:
    def __init__(self, matrix):
        self._tmp1 = matrix.createVecRight()
        self._tmp2 = matrix.createVecRight()
        self._matrix = matrix

    def calculate(self, u, f):
        # tmp1 = A u
        self._matrix.mult(u, self._tmp1)
        # tmp2 = b
        f.copy(self._tmp2)
        # tmp2 = b - Au
        self._tmp2.axpy(-1, self._tmp1)
        return self._tmp2.norm()
