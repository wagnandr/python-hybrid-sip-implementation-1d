import numpy as np
import argparse
from matplotlib import pyplot as plt
from math import (sin, pi)
from core import (create_mesh, init_matrix, assemble, assemble_mass, plot, interpolate, add_dirichlet, ErrorCalculator,
                  LUSolver, Jacobi)


def run(level, macro_boundaries, solver, sigma, frequency, show_plot):
    N = 2 ** level + 1
    M = len(macro_boundaries) - 1

    coords = create_mesh(macro_boundaries, N, M)

    K = assemble(sigma, coords, N, M)

    u, f = K.getVecs()
    u_val = lambda t, _: sin(2 * pi * frequency * t)
    f_val = lambda t, _: (2 * pi*frequency )** 2 * sin(2 * pi * frequency * t)

    f_prime = K.getVecLeft()

    interpolate(f_val, coords, f_prime, level, M)
    mass_matrix = init_matrix(N, M)
    assemble_mass(mass_matrix, coords, N, M)
    mass_matrix.assemble()
    mass_matrix.mult(f_prime, f)

    add_dirichlet(K, f, u_val, coords, N, M)

    solver.setup_blocks(K, M-1, N)
    solver.solve(u, f)

    error_calculator = ErrorCalculator(mass_matrix, coords, level, M)
    error = error_calculator.calculate(u, u_val)

    if show_plot:
        plot(u, coords, N, M)
        plt.show()

    return error, N * M


parser = argparse.ArgumentParser(description='Run the hdG test problem in 1D.')
parser.add_argument('--macroboundaries', help='position of the boundaries of the macrointervals', nargs='+', type=float,
                    default=[0, 0.5, 1])
parser.add_argument('--level', help='which level should we use?', type=int, default=3)
parser.add_argument('--sigma', help='which value for sigma', type=float, default=6)
parser.add_argument('--frequency', help='which value for sigma', type=float, default=6)
parser.add_argument('--show-plots', help='should the plots be displayed?', action='store_true')
parser.add_argument('--error-scaling', help='should a scaling plot be used?', action='store_true')
parser.add_argument('--use-jacobi', help='should the jacobi smoother be used?', action='store_true')
args = parser.parse_args()

macro_boundaries = args.macroboundaries

if args.use_jacobi:
    solver = Jacobi(num_smoothing_steps=1000000)
else:
    solver = LUSolver()

if args.error_scaling:
    list_errors = []
    list_num_dofs = []
    for level in range(1, args.level):
        error, num_dofs = run(level, args.macroboundaries, solver, args.sigma, args.frequency, args.show_plots)
        list_errors.append(error)
        list_num_dofs.append(num_dofs)
    plt.loglog(list_num_dofs, list_errors, label='$L^2$-error')
    # line for comparison
    prefactor = np.array(list_num_dofs)[1] ** 2 * list_errors[2]
    plt.loglog(list_num_dofs[1:-1], prefactor * 1 / np.array(list_num_dofs)[1:-1] ** 2, ':', label='slope -2')
    plt.legend()
    plt.grid(True)
    plt.show()
else:
    error, num_dofs = run(args.level, args.macroboundaries, solver, args.sigma, args.frequency, args.show_plots)
    print("on level={}, error={}".format(args.level, error))
