from math import sqrt, pi
import os
import sys, slepc4py
import numpy as np

slepc4py.init(sys.argv)
from slepc4py import SLEPc
import argparse
from matplotlib import pyplot as plt
from core import (create_mesh, assemble, plot, add_dirichlet_matrix, init_matrix, assemble_mass, Jacobi, BlockJacobi)
from utils import set_width_in_percent

parser = argparse.ArgumentParser(description='Run the hdG test problem in 1D.')
parser.add_argument('--macroboundaries', help='position of the boundaries of the macrointervals', nargs='+', type=float,
                    default=[0, 0.5, 1])
parser.add_argument('--num-microintervals', help='how many microintervals should we have?', type=int, default=6)
parser.add_argument('-dbv', '--dirichlet-boundary-values', help='apply dirichlet boundary values?', action='store_true')
parser.add_argument('--sigma', help='which value for sigma', type=float, default=6)
parser.add_argument('--weight', help='smoothing weight', type=float, default=2./3.)
parser.add_argument('--num-smoothing-steps', help='how many jacobi iterations should we apply', nargs='+', type=int, 
                    default=[1])
parser.add_argument('-o', '--output-directory', help='output directory to save the plots', type=str, required=False)
parser.add_argument('--hide-plots', help='hide the plots', action='store_true')
parser.add_argument('--use-mass', help='include the mass matrix in our calculation', action='store_true')
parser.add_argument('--use-blocks', help='include the mass matrix in our calculation', action='store_true')
parser.add_argument('--title', help='the plot title', type=str, default='')
args = parser.parse_args()

macro_boundaries = args.macroboundaries

N = args.num_microintervals + 1
M = len(macro_boundaries) - 1

coords = create_mesh(macro_boundaries, N, M)

sigma = args.sigma
weight = args.weight

matrix = assemble(sigma, coords, N, M)

mass_matrix = init_matrix(N, M)
assemble_mass(mass_matrix, coords, N, M)
mass_matrix.assemble()

if args.dirichlet_boundary_values:
    add_dirichlet_matrix(matrix, N, M)

E = SLEPc.EPS()
E.create()

if args.use_mass:
    E.setOperators(matrix, mass_matrix)
else:
    E.setOperators(matrix)

problemType = None
if args.dirichlet_boundary_values:
    if args.use_mass:
        problemType = SLEPc.EPS.ProblemType.GNHEP
    else:
        problemType = SLEPc.EPS.ProblemType.NHEP
else:
    if args.use_mass:
        problemType = SLEPc.EPS.ProblemType.GHEP
    else:
        problemType = SLEPc.EPS.ProblemType.HEP

E.setProblemType(problemType)
E.setDimensions(nev=N * M)
E.setFromOptions()

E.solve()

eps_type = E.getType()
print("solution method: {}".format(eps_type))
nconv = E.getConverged()
print("number of converged eigenpairs {}".format(nconv))

u, f = matrix.getVecs()

set_width_in_percent(0.5, 1.5, False)

max_large_rate = 0

for num_smoothing_steps in args.num_smoothing_steps:
    if nconv > 0:
        # Create the results vectors
        vr, wr = matrix.getVecs()
        vi, wi = matrix.getVecs()

        x = np.zeros(nconv)
        y = np.zeros(nconv)

        for idx in range(nconv):
            k = E.getEigenpair(idx, vr, vi)

            if args.use_blocks:
                solver = BlockJacobi(M-1, N, weight=weight, atol=1e-12, num_smoothing_steps=num_smoothing_steps, verbose=False)
            else:
                solver = Jacobi(weight=weight, atol=1e-12, num_smoothing_steps=num_smoothing_steps, verbose=False)
            solver.setup(matrix)

            vr.copy(u)

            conv_rate, residuum, rates = solver.solve(u, f)

            x[idx] = k.real
            #y[idx] = conv_rate
            y[idx] = rates[-1] 
            print(residuum)

        y = y[np.argsort(x)]
        max_large_rate = max(y[-1], max_large_rate)

        color = '#e5000033' if num_smoothing_steps == 11 else None

        plt.plot(y, color=color, label ='{} smoothing step{}'.format(num_smoothing_steps, '' if num_smoothing_steps < 2 else 's'))

plt.axhline(max_large_rate, color='#e5000033', linestyle=':')
plt.axvline(nconv/2, color='gray', linestyle=':')
plt.legend(loc='upper right')
plt.title(args.title)
plt.grid(True)
plt.xlabel('$k^{th}$ eigenvalue')
plt.ylabel('convergence rate')
plt.ylim([0, 1])
plt.tight_layout()
plt.savefig('results/smoothing-rates.pgf')
plt.show()
