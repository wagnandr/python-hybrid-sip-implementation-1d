from math import sqrt, pi
import os
import sys, slepc4py
import numpy as np

slepc4py.init(sys.argv)
from slepc4py import SLEPc
import argparse
from matplotlib import pyplot as plt
from core import (create_mesh, assemble, plot, add_dirichlet_matrix, init_matrix, assemble_mass, Jacobi, BlockJacobi)
from utils import (set_width_in_percent)

set_width_in_percent(1, 0.5, False)

parser = argparse.ArgumentParser(description='Run the hdG test problem in 1D.')
parser.add_argument('--macroboundaries', help='position of the boundaries of the macrointervals', nargs='+', type=float,
                    default=[0, 0.5, 1])
parser.add_argument('--num-microintervals', help='how many microintervals should we have?', type=int, default=6)
parser.add_argument('-dbv', '--dirichlet-boundary-values', help='apply dirichlet boundary values?', action='store_true')
parser.add_argument('--sigma', help='which value for sigma', type=float, default=6)
parser.add_argument('--num-smoothing-steps', help='how many jacobi iterations should we apply', nargs='+', type=int, 
                    default=[0, 1])
parser.add_argument('-o', '--output-directory', help='output directory to save the plots', type=str, required=False)
parser.add_argument('--hide-plots', help='hide the plots', action='store_true')
parser.add_argument('--use-blocks', help='use a block smoother', action='store_true')
parser.add_argument('--use-mass', help='include the mass matrix in our calculation', action='store_true')
args = parser.parse_args()

macro_boundaries = args.macroboundaries

N = args.num_microintervals + 1
M = len(macro_boundaries) - 1

coords = create_mesh(macro_boundaries, N, M)

sigma = args.sigma

matrix = assemble(sigma, coords, N, M)

mass_matrix = init_matrix(N, M)
assemble_mass(mass_matrix, coords, N, M)
mass_matrix.assemble()

if args.dirichlet_boundary_values:
    add_dirichlet_matrix(matrix, N, M)

E = SLEPc.EPS()
E.create()

if args.use_mass:
    E.setOperators(matrix, mass_matrix)
else:
    E.setOperators(matrix)

problemType = None
if args.dirichlet_boundary_values:
    if args.use_mass:
        problemType = SLEPc.EPS.ProblemType.GNHEP
    else:
        problemType = SLEPc.EPS.ProblemType.NHEP
else:
    if args.use_mass:
        problemType = SLEPc.EPS.ProblemType.GHEP
    else:
        problemType = SLEPc.EPS.ProblemType.HEP

E.setProblemType(problemType)
E.setDimensions(nev=N * M)
E.setFromOptions()

E.solve()

eps_type = E.getType()
print("solution method: {}".format(eps_type))
nconv = E.getConverged()
print("number of converged eigenpairs {}".format(nconv))


def apply_jacobi(num_smoothing_steps, u, f):
    u = u.copy()
    if num_smoothing_steps <= 0:
        return u
    if args.use_blocks:
        solver = BlockJacobi(M-1, N, weight=2./3., atol=1e-15, num_smoothing_steps=num_smoothing_steps, verbose=False)
    else:
        solver = Jacobi(weight=2./3., atol=1e-15, num_smoothing_steps=num_smoothing_steps, verbose=False)
    solver.setup(matrix)
    conv_rate = solver.solve(u, f)
    return u


colors = ['red', 'green', 'blue', 'orange']

if nconv > 0:
    # Create the results vectors
    vr, wr = matrix.getVecs()
    vi, wi = matrix.getVecs()

    x = np.zeros(nconv)
    y = np.zeros(nconv)

    for idx in range(nconv):
        k = E.getEigenpair(idx, vr, vi)

        u = vr.copy()
        f = 0 * vr.copy()

        for sidx, smoothing_steps in enumerate(args.num_smoothing_steps):
            u_smoothed = apply_jacobi(smoothing_steps, u, f)
            color = colors[sidx % len(colors)]
            if sidx == 0:
                label = 'no smoothing'
            elif sidx == 1:
                label = '1 smoothing step'
            else:
                label = '{} smoothing steps'.format(smoothing_steps)
            plot(u_smoothed, coords, N, M, color=color, label=label)

        for pt in args.macroboundaries[1:-1]:
            plt.axvline(pt, color='gray', linestyle=':')

        print(idx)
        plt.legend()
        # plt.title('smoothing behaviour mode {}'.format(idx))
        plt.savefig('results/smooth-modes-{}.pgf'.format(idx))
        plt.show()
