# About
A hybrid symmetric interior penalty (SIP) method implemented in 1D for a master's thesis.

For solving the linear system both a pointwise-Jacobi method and a block-Jacobi method were implemented inside a complete V-cycle based multigrid algorithm.
This was used to get an idea why the performance of a hybrid SIP method deteriorates for large penalty parameters.

In addition, the eigenvalues and eigenfunctions of the discretized operators are investigated to get an idea how the spuriuous and conforming eigenvectors of the formulation look like.

# Prerequisits

All scripts need
- numpy
- petsc4py
- matplotlib

for execution. The scripts concerned with eigenvalues and eigenvectors need in addition
- slepc4py

# Usage
The parameters of all scripts can be queried with a ```-h``` parameter.

A few interesting parameter combinations are

```
python eigenfunction_smooth_modes.py --num-microintervals 16 --macroboundaries 0 0.5 1 --num-smoothing-steps 0 1 2 3 --use-mass --sigma 6e2 --use-blocks
```
```
python eigenvalues.py --macroboundaries 0 0.5 1 --num-microintervals 32 --use-jacobi -dbv --sigma 6e10
```
```
python itermatrix.py --level 7 --macroboundaries 0 0.3 0.7 1
```
```
python multigrid.py --macroboundaries 0 0.25 0.5 0.75 1 --sigma 6000 --level 11 --num-v-cycles 80 --show-plots
```
```
python eigenfunction_smooth_error.py --level 6 --macroboundaries 0 0.5 1 -dbv --sigma 6 --num-smoothing-steps 0 1 5 10 --smoother-type block-jacobi --plot-type rates
```
```
python eigenfunction_smoothing_rates.py --num-microintervals 16 --macroboundaries 0 0.5 1 --num-smoothing-steps 1 2 3 15 --use-mass --sigma 6e2 --use-blocks
```