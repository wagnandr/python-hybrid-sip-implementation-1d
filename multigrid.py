import argparse
from matplotlib import pyplot as plt
from core import (init_matrix, plot, assemble_mass, MultigridSolver, ErrorCalculator, ResiduumCalculator)
from math import (sin, pi)

parser = argparse.ArgumentParser(description='Run the hdG test problem in 1D.')
parser.add_argument('--macroboundaries', help='position of the boundaries of the macrointervals', nargs='+', type=float,
                    default=[0, 0.5, 1])
parser.add_argument('--level', help='which level should we use?', type=int, default=3)
parser.add_argument('--sigma', help='which value for sigma', type=float, default=6)
parser.add_argument('--frequency', help='sine frequency', type=float, default=1)
parser.add_argument('--num-v-cycles', help='how many v-cycles should be applied', type=int, default=10)
parser.add_argument('--show-plots', help='should the plots be displayed?', action='store_true')
args = parser.parse_args()

k = args.frequency
u_val = lambda t, _: sin(2 * k * pi * t)
f_val = lambda t, _: (2 * pi * k) ** 2 * sin(2 * k * pi * t)

macro_boundaries = args.macroboundaries
max_level = args.level
sigma = args.sigma

mg_solver = MultigridSolver(max_level, macro_boundaries, sigma, f_val, u_val)

mass_matrix = init_matrix(mg_solver.num_microdofs[-1], mg_solver.num_macrointervals)
assemble_mass(mass_matrix, mg_solver.list_meshes[-1], mg_solver.num_microdofs[-1], mg_solver.num_macrointervals)
mass_matrix.assemble()
error_calculator = ErrorCalculator(mass_matrix, mg_solver.list_meshes[-1], max_level, mg_solver.num_macrointervals)


def plot_last_u():
    if args.show_plots:
        plot(mg_solver.list_u[-1], mg_solver.list_meshes[-1], mg_solver.num_microdofs[-1], mg_solver.num_macrointervals)
        plt.show()


plot_last_u()
residuum_calculator = ResiduumCalculator(mg_solver.list_K[-1])
last_residuum = residuum_calculator.calculate(mg_solver.list_u[-1], mg_solver.list_f[-1])
last_error = error_calculator.calculate(mg_solver.list_u[-1], u_val)
print('residuum = {}, error = {}'.format(last_residuum, last_error))
for idx in range(args.num_v_cycles):
    u = mg_solver.solve()
    plot_last_u()
    current_residuum = residuum_calculator.calculate(mg_solver.list_u[-1], mg_solver.list_f[-1])
    residuum_rate = current_residuum / last_residuum
    last_residuum = current_residuum
    error = error_calculator.calculate(mg_solver.list_u[-1], u_val)
    print('residuum = {}, residuum-rate = {}, error = {}'.format(current_residuum, residuum_rate, error))
