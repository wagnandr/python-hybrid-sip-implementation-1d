from math import sqrt, pi
import os
import sys, slepc4py

slepc4py.init(sys.argv)
from slepc4py import SLEPc
import argparse
from matplotlib import pyplot as plt
from core import (create_mesh, assemble, plot, add_dirichlet_matrix, init_matrix, assemble_mass, Jacobi)

parser = argparse.ArgumentParser(description='Run the hdG test problem in 1D.')
parser.add_argument('--macroboundaries', help='position of the boundaries of the macrointervals', nargs='+', type=float,
                    default=[0, 0.5, 1])
parser.add_argument('--num-microintervals', help='how many microintervals should we have?', type=int, default=6)
parser.add_argument('-dbv', '--dirichlet-boundary-values', help='apply dirichlet boundary values?', action='store_true')
parser.add_argument('--sigma', help='which value for sigma', type=float, default=6)
parser.add_argument('-o', '--output-directory', help='output directory to save the plots', type=str, required=False)
parser.add_argument('--hide-plots', help='hide the plots', action='store_true')
parser.add_argument('--use-mass', help='include the mass matrix in our calculation', action='store_true')
parser.add_argument('--use-jacobi', help='Use D^{-1}A instead of A', action='store_true')
args = parser.parse_args()

macro_boundaries = args.macroboundaries

N = args.num_microintervals + 1
M = len(macro_boundaries) - 1

coords = create_mesh(macro_boundaries, N, M)

sigma = args.sigma

matrix = assemble(sigma, coords, N, M)

mass_matrix = init_matrix(N, M)
assemble_mass(mass_matrix, coords, N, M)
mass_matrix.assemble()

if args.use_jacobi:
    jac = Jacobi(weight=2./3., atol=1e-15, num_smoothing_steps=1, verbose=False)
    jac.setup(matrix)
    matrix = jac._iter_matrix

if args.dirichlet_boundary_values:
    add_dirichlet_matrix(matrix, N, M)

E = SLEPc.EPS()
E.create()

if args.use_mass:
    E.setOperators(matrix, mass_matrix)
else:
    E.setOperators(matrix)

problemType = None
if args.dirichlet_boundary_values:
    if args.use_mass:
        problemType = SLEPc.EPS.ProblemType.GNHEP
    else:
        problemType = SLEPc.EPS.ProblemType.NHEP
else:
    if args.use_mass:
        problemType = SLEPc.EPS.ProblemType.GHEP
    else:
        problemType = SLEPc.EPS.ProblemType.HEP

E.setProblemType(problemType)
E.setDimensions(nev=N * M)
E.setFromOptions()

E.solve()

eps_type = E.getType()
print("solution method: {}".format(eps_type))
nconv = E.getConverged()
print("number of converged eigenpairs {}".format(nconv))

if nconv > 0:
    # Create the results vectors
    vr, wr = matrix.getVecs()
    vi, wi = matrix.getVecs()

    for idx in range(nconv):
        k = E.getEigenpair(idx, vr, vi)
        error = E.computeError(idx)
        if k.imag != 0.0:
            print(" %9f%+9f j %12g" % (k.real, k.imag, error))
        else:
            title = "{}: real={}    error={}".format(idx, k.real, error)
            print(title)
            plt.title(title)
        plot(vr, coords, N, M)
        if args.output_directory is not None:
            plt.savefig(os.path.join(args.output_directory, 'eigenvalue_{:03d}.png'.format(idx)))
        if not args.hide_plots:
            plt.show()
        else:
            plt.clf()
